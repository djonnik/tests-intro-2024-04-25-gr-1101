def get_tricky_reversed_words(word: str) -> str:
    """Get reversed word (alpha symbols - revers, not-alpha - stay on old places).

    Usage examples:
    >>> get_tricky_reversed_words('asdf')
    'fdsa'
    >>> get_tricky_reversed_words('')
    ''
    >>> get_tricky_reversed_words(3)
    Traceback (most recent call last):
    ...
    TypeError: unsupported type: <class 'int'>. Expected 'str'.
    """
    if not isinstance(word, str):
        raise TypeError(f"unsupported type: {type(word)}. Expected 'str'.")
    inverted_word = ""
    letters = [letter for letter in word if letter.isalpha()]
    for symbol in word:
        if symbol.isalpha():
            inverted_word += letters.pop()
        else:
            inverted_word += symbol
    return inverted_word


def tricky_revers(text: str) -> str:
    words = text.split()
    reversed_words = map(get_tricky_reversed_words, words)
    return " ".join(reversed_words)


if __name__ == "__main__":
    import doctest
    doctest.testmod()